#!/bin/bash

function run_sequence(){
    RESULTS_DIR=/root/newResults-`uname -r`_ALLCPUS-`lscpu  | grep "CPU(s):" | head -1 | awk '{print $2}'`_OnlineCPUs-`lscpu | grep CPU | grep On-line | awk '{print $4}'`

    echo $RESULTS_DIR
    [ ! -e $RESULTS_DIR ] && mkdir $RESULTS_DIR

    docker run --rm -it  -v /root/x509up_u4719:/tmp/x509up_u0 --rm --cpus=2 --cpuset-cpus=1-2  gitlab-registry.cern.ch/giordano/simple-bmk:latest -b better-test | tee $RESULTS_DIR/better-test-01.txt

    docker run --rm -it  -v /root/x509up_u4719:/tmp/x509up_u0 --rm --cpus=2 --cpuset-cpus=1-2  gitlab-registry.cern.ch/giordano/simple-bmk:latest -b better-test | tee $RESULTS_DIR/better-test-02.txt

    docker run --rm -it  -v /root/sysbench-data:/sysbench-data -v /root/x509up_u4719:/tmp/x509up_u0 --rm --cpus=2 --cpuset-cpus=1-2  gitlab-registry.cern.ch/giordano/simple-bmk:latest -b sysbench-io | tee $RESULTS_DIR/sysbench-io.txt
}

function all_siblings_online(){
    for i in {24..47}; do echo 1 > /sys/devices/system/cpu/cpu$i/online; done
}

function all_siblings_offline(){
    for i in {24..47}; do echo 0 > /sys/devices/system/cpu/cpu$i/online; done
}

function socket0_siblings_offline(){
    for i in {24..35}; do echo 0 > /sys/devices/system/cpu/cpu$i/online; done
    for i in {36..47}; do echo 1 > /sys/devices/system/cpu/cpu$i/online; done
}

function socket1_siblings_offline(){
    for i in {24..35}; do echo 1 > /sys/devices/system/cpu/cpu$i/online; done
    for i in {36..47}; do echo 0 > /sys/devices/system/cpu/cpu$i/online; done
}

all_siblings_online 2> /dev/null
run_sequence

all_siblings_offline 2> /dev/null
[[ "$?" == 0 ]] && run_sequence

socket0_siblings_offline 2> /dev/null
[[ "$?" == 0 ]] && run_sequence

socket1_siblings_offline 2> /dev/null
[[ "$?" == 0 ]] && run_sequence
