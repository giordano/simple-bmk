FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

COPY egi.repo /etc/yum.repos.d/egi.repo

RUN mkdir /mypackages; yum install --downloadonly --downloaddir=/mypackages sysbench ; yum clean all

RUN yum install -y \
    globus-proxy-utils \
   ca-policy-lcg ca_policy_igtf-classic ca-policy-egi-core \
   condor-python ; yum clean all

RUN yum install -y stress fetch-crl; yum clean all

RUN yum install -y sysbench; yum clean all

COPY better-test /root
COPY bmk.sh /root
ENTRYPOINT ["/root/bmk.sh"]