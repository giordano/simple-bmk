# simple-bmk

This simple docker container enables (for now) two benchmarks

* stress CPU
  * this will run for 60 seconds a cpu stress test on a configurable number of cores (-c <number_cores>)
   * Example to run is 
   ``docker run simplebmk:latest -b stress``

* a condor scheduler query test (default). It will run for 10 seconds. The duration of the first test is indicative of issues if above 0.7 secs
   * This is the default process. It requires that a grid proxy certificate is made available to the container.
   * Example to run is 
   ``docker run -v /tmp/grid_proxy:/tmp/x509up_u0 simplebmk:latest ``

* sysbench
  * CPU test: ``docker run simplebmk:latest -b sysbench-cpu``
  * IO test: ``docker run simplebmk:latest -b sysbench-io``
