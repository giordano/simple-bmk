#!/bin/bash

TINTERVAL=2
MAXSEQ=$((60/TINTERVAL -1 ))
DOUBLE_LOADAVG=60
for i in `seq 0 $MAXSEQ`;
do
echo "[$i: `date`]"
    if [ $(( ( RANDOM % 100 )  + 1 )) -lt $DOUBLE_LOADAVG ]; then
        stress -c 2 -t $TINTERVAL
    else
        sleep $TINTERVAL
    fi
done
