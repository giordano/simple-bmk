#!/bin/bash

copies=`nproc`
benchmark='better-test'
while getopts "b:c:h" o; do
  case ${o} in
    b)
      benchmark=$OPTARG
      ;;
    c) 
      [ $OPTARG -gt 0 ] && copies=$OPTARG
      ;;
    h)
      echo "usage: $0 -b <better-test|stress [$benchmark]> [-c <number of copies [$copies]>] "
      exit 0
      ;;
  esac
done

echo "benchmark $benchmark"
[ "$benchmark" == "better-test" ] && chown root /tmp/x509up_u0 && timeout 60 python /root/better-test
[ "$benchmark" == "stress" ] && stress -c $copies -t 60
[ "$benchmark" == "sysbench-cpu" ] && sysbench --cpu-max-prime=200000 --num-threads=$copies  cpu run
if [ "$benchmark" == "sysbench-io" ]; then
    [ ! -e /sysbench-data ] && mkdir /sysbench-data
    cd /sysbench-data
    FileSize=`free -g | grep Mem | awk '{print 2*$2}'`
    echo "Total file size $FileSize"
    [ `ls -1 test_file* | wc -l` -eq 0 ] && sysbench --file-total-size="$FileSize"G fileio prepare
    sysbench --file-total-size="$FileSize"G --file-test-mode=rndrw --time=300 fileio run
    #sysbench --file-total-size="$FileSize"G fileio cleanup
fi


